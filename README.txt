$Id

Faceted Search for Google Search Appliance (gsa_faceted_search)
by Lars Kleiner

Provides a Google Search Appliance search engine for Faceted Search.

This module is the glue between Faceted Search and Google Search Appliance.
While the categories (taxonomy facets, content type facets, etc.) are still
retrieved from Drupal, the search results get served up by GSA.

Requirements
The Faceted Search module needs to be decoupled from the Drupal
search by applying patch http://drupal.org/node/230415#comment-1882680. This
patch shouldn't break current Faceted Search installs, it allows
users to switch between search engines for any given Faceted Search
environment.

Features
Implemented are most of the core Faceted Search methods and facets, including
- author facet
- content type facet
- taxonomy facet

To Do / Issues
- Search results sorting needs to be added
- The date authored facet hasn't been tested
- Only the extract faceted_search_ui style works
- Faceted Search Views integration is not implemented

Credits
Sponsored by Digital People Online (http://digitalpeopleonline.com)
