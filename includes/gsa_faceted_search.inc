<?php

/**
 * @file
 * The Google Search Appliance Faceted Search implementation
 */

/**
 * Require faceted_search and google_appliance base classes
 */
require_once('./'. drupal_get_path('module', 'faceted_search') .'/includes/search.inc');
require_once('./'. drupal_get_path('module', 'google_appliance') .'/DrupalGoogleMini.php');

/**
 * The keyword AND category.
 */
class faceted_search_gsa_faceted_search_keyword_and_category extends faceted_search_keyword_and_category {

 /**
  * Inject components into the query for selecting nodes matching this category.
  *
  * @param $query
  *   Query to inject the components into.
  * @param $words
  *   Array keyed by search index type, each element being an array of positive
  *   words to lookup for that index type. This method should insert any words
  *   it cares about.
  * @param $matches
  *   Minimum number of words that should match in query results for each index type.
  * @param $type
  *   Type of search index entry to be searched.
  */
  function build_results_query(&$query, &$words, &$matches, $type) {
    if (($word = $this->check_word($this->_word)) && !isset($words[$type][$word])) {
      if (strlen($word) >= variable_get('minimum_word_size', 3)) {
        $words[$type][$word] = $word;
        $matches[$type]++;
      }
      else {
        // Ensure this type will be searched even though it has no "long" word.
        if (!isset($words[$type])) {
          $words[$type] = array();
        }
      }
    }
  }
}


/**
 * The keyword phrase category.
 */
class faceted_search_gsa_faceted_search_keyword_phrase_category extends faceted_search_keyword_phrase_category {

  /**
   * Inject components into the query for selecting nodes matching this category.
   *
   * @param $query
   *   Query to inject the components into.
   * @param $words
   *   Array keyed by search index type, each element being an array of positive
   *   words to lookup for that index type. This method should insert any words
   *   it cares about.
   * @param $matches
   *   Minimum number of words that should match in query results for each index type.
   * @param $type
   *   Type of search index entry to be searched.
   */
  function build_results_query(&$query, &$words, &$matches, $type) {
    $split = explode(' ', $this->_phrase);
    foreach ($split as $word) {
      if ($word = $this->check_word($word)) {
        $words[$type][$word] = $word;
      }
    }
    if (count($split) > 0) {
      $matches[$type]++; // A phrase counts as one match.
    }
  }
}


/**
 * The keyword OR category.
 */
class faceted_search_gsa_faceted_search_keyword_or_category extends faceted_search_keyword_or_category {

  /**
   * Return the search text for this category.
   */
  function get_text() {
    return implode(' OR ', $this->_words);
  }

  /**
   * Inject components into the query for selecting nodes matching this category.
   *
   * @param $query
   *   Query to inject the components into.
   * @param $words
   *   Array keyed by search index type, each element being an array of positive
   *   words to lookup for that index type. This method should insert any words
   *   it cares about.
   * @param $matches
   *   Minimum number of words that should match in query results for each index type.
   * @param $type
   *   Type of search index entry to be searched.
   */
  function build_results_query(&$query, &$words, &$matches, $type) {
    foreach ($this->_words as $word) {
      if (($word = $this->check_word($word)) && !isset($words[$type][$word])) {
        $words[$type][$word] = $word;
      }
    }
  }
}


/**
 * The keyword NOT category.
 */
class faceted_search_gsa_faceted_search_keyword_not_category extends faceted_search_keyword_not_category {

  /**
   * Inject components into the query for selecting nodes matching this category.
   *
   * @param $query
   *   Query to inject the components into.
   * @param $words
   *   Array keyed by search index type, each element being an array of positive
   *   words to lookup for that index type. This method should insert any words
   *   it cares about.
   * @param $matches
   *   Minimum number of words that should match in query results for each index type.
   * @param $type
   *   Type of search index entry to be searched.
   */
  function build_results_query(&$query, &$words, &$matches, $type) {
    if ($word = $this->check_word($this->_word)) {
      // This is a negative word; do not insert it, but mark the type as used.
      if (!isset($words[$type])) {
        $words[$type] = array();
      }
    }
  }
}


/**
 * This class stores and processes data related to a search.
 */
class faceted_search_gsa_faceted_search extends faceted_search {
  var $_google_appliance_name;
  var $_default_client;
  var $_default_collection;
  var $_tabs_array;
  var $_default_tab;
  var $_default_search_path;
  var $_results;
  var $_query;

 /**
  * Constructor. Initialize the search environment.
  *
  * @param $record
  *   Optional for this environment, as fetched from the database. Defaults to
  *   NULL (for new environment).
  */
  function faceted_search_gsa_faceted_search($record = NULL) {
    $settings = google_appliance_get_settings();
    $this->_google_appliance_name = $settings['google_appliance_name'];
    $this->_default_client = $settings['default_client'];
    $this->_default_collection = $settings['default_collection'];
    $this->_tabs_array = $settings['tabs_array'];
    $this->_default_tab = $settings['default_tab'];
    $this->_default_search_path = $settings['default_search_path'];
    parent::faceted_search($record);
  }

 /**
  * Prepare the complete search environment (with its filters), parsing the
  * given search text. Requires that an env_id has been assigned previously.
  *
  * @param $text
  *   Optional search text. Defaults to the empty string.
  * @return
  *   TRUE is the search environment could be successfully built.
  */
  function prepare($text = '') {
    if (!$this->env_id) {
      return FALSE;
    }
    $this->_text = $text;
    
    // Load settings for all enabled filters in this search environment.
    $all_filter_settings = faceted_search_load_filter_settings($this);

    // Make a selection with all enabled filters.
    $selection = faceted_search_get_filter_selection($all_filter_settings);

    // Collect all filters relevant to this search.
    foreach (module_implements('faceted_search_collect') as $module) {
      $module_filters = array();
      $hook = $module .'_faceted_search_collect';

      // Parse the search text and obtain corresponding filters. Text is eaten as
      // it gets parsed.
      $text = $hook($module_filters, 'text', $this, $selection, $text);

      // Disallow filters that already have been collected from the search text.
      foreach ($module_filters as $filter) {
        unset($selection[$filter->get_key()][$filter->get_id()]);
      }

      // Collect any remaining allowed facets.
      if (!empty($selection)) {
        $hook($module_filters, 'facets', $this, $selection);
      }

      // Merge the filters listed by the current module.
      $this->_filters = array_merge($this->_filters, $module_filters);

      if (empty($selection)) {
        break; // No more filters allowed.
      }
    }

    // After filters have been collected, any remaining text is passed to the
    // node filters.
    faceted_search_collect_node_keyword_filters($this->_filters, 'text', $this, $text);

    // Prepare filters for use, assigning them their settings are sorting them.
    faceted_search_prepare_filters($this->_filters, $all_filter_settings);

    // Assign the keywords found.
    foreach ($this->_filters as $filter) {
      $filter->get_keywords($this->_keywords);
    }

    return TRUE;
  }
 
  /**
   * Perform the search.
   *
   * The prepare() method must have been called previously.
   */
  function execute() {
    if (!$this->_filters) {
      return; // Nothing to search
    }

    $query = new faceted_search_gsa_faceted_search_query();

    if (!$this->settings['ignore_status'] || !user_access('administer nodes')) {
      // Restrict the search to published nodes only.
      $query->addMetaDataFilter('status', 1);
    }

    // Apply node type filter
    $types = faceted_search_types($this);
    if (!empty($types)) {
      $query->addMetaDataFilter('type', $types);
    }
    
    foreach ($this->_filters as $filter) { // TODO: All filters are iterated; We should avoid iterating through those that are disabled.		
      $filter->build_results_query($query);
    }
    $this->_query = $query;
    $this->_ready = TRUE;
  }


  /**
   * Fetch the items from the current search results.
   *
   * execute() must have been called beforehand.
   *
   * @return
   *   Array of objects with search results.
   */
  function load_results() {
    $keys = array_shift($this->_keywords);
    $this->_query->setKeywords($keys);
    $this->_results = gsa_faceted_search_view(
      $this->_query,
      $this->_default_search_path,
      $this->_default_client,
      $this->_default_collection,
      $keys
    );
    $this->_results_count = count($this->_results);
    return $this->_results;
  }

  /**
   * Return this search's results.
   */
  function get_results() {
    return $this->_results;
  }
  
  /**
   * Return this search's query.
   */
  function get_query() {
    return $this->_query;
  }

 /**
  * Return the categories for the given facet and count matching nodes within
  * results.
  *
  * @param $facet
  *   The facet whose categories are to be loaded.
  * @param $from
  *   Ordinal number of the first category to load. Numbering starts at 0.
  * @param $max_count
  *   Number of categories to load.
  * @return
  *   Array of categories (objects having the faceted_search_category
  *   interface).
  */
  function load_categories($facet, $from = NULL, $max_count = NULL) {
    $results = array();

    if ($this->_results_count > 0) {
      // Search within results.
     $query = drupal_clone($this->_query);
    }
    else {
      // Current search yields no results, thus no categories are possible.
      return array();
    }

    // Gather the query components that will retrieve the categories.
    if ($active_category = $facet->get_active_category()) {
      $has_categories = $active_category->build_subcategories_query($query);
    }
    else {
      $has_categories = $facet->build_root_categories_query($query);
    }
    if (!$has_categories) {
      return array();
    }

    // Apply node type filter
    $types = faceted_search_types($this);
    if (!empty($types)) {
      $this->_query->addMetaDataFilter('type', $types);
    }

    $category = $facet->get_category($this);
    foreach ($category['values'] as $key => $value) {
      $query->addMetaDataFilter($category['category'], $value);
      if ($query->debug) {
        $query_parts = print_r($query->getQueryParts(), TRUE);  
        $query->log(get_class() .': '. get_class($facet) .': '. $query_parts);
      }
      try {
        $results[$category['name']][$key] = array(
          'value' => $value,
          'count' => count((array) $query->query()),
        );
      }
      catch (GoogleMiniResultException $e) {
        continue;
      }
    }

    return $facet->build_categories($results);
  }
}

/**
 * The faceted search query wrapper for the Google Mini class
 */
class faceted_search_gsa_faceted_search_query extends DrupalGoogleMini {
  function faceted_search_gsa_faceted_search_query() {
    $google_debug = variable_get('google_appliance_debug', 0);
    if ($google_debug >= 2 ) {
      parent::__construct(TRUE, 'dpr');
    }
    elseif ($google_debug == 1) {
      parent::__construct(TRUE);
    }
    else {
      parent::__construct(FALSE);
    }
    _google_appliance_search_initialise($this);
  }
}
