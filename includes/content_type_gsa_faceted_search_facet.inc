<?php

/**
 * @file
 * The content type facet implementation for Google Search Appliance
 */

/**
 * A node-type based facet.
 */
class content_type_gsa_faceted_search_facet extends content_type_facet {

  /**
   * @param $search
   *  The faceted search object
   *
   * @return $category
   *  The category Array
   */
  function get_category($search) {
    $category = array();
    $category['category'] = 'type';
    $category['name'] = 'Type';
    $category['values'] = $this->_get_content_types($search);
    return $category;
  }

  /**
   * @param $search
   *  The faceted search object
   *
   * @return $users
   *  Array of content types
   */
  function _get_content_types($search) {
    $types = array();
    $node_types = faceted_search_types($search);
    foreach (array_keys($node_types) as $type) {
      $name = ucfirst($type);
      $types[$name] = $type;
    }
    return $types;
  }

  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results.
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    if (!empty($this->_types)) {
      $query->addMetaDataFilter('type', $this->_types);
    }
    return TRUE;
  }

  /**
   * This factory method creates categories given query results that include the
   * fields selected in get_root_categories_query() or get_subcategories_query().
   *
   * @param $results
   *   $results A database query result resource.
   *
   * @return
   *   Array of categories.
   */
  function build_categories($results) {
    $categories = array();
    foreach ($results as $result) {
      foreach ($result as $key => $value) {
        $categories[] = new content_type_gsa_faceted_search_facet_category($value['value'], $key, $value['count']);
      }
    }
    return $categories;
  }
}


/**
 * A node-type based facet category.
 */
class content_type_gsa_faceted_search_facet_category extends content_type_facet_category {
  
  /**
   * Updates a query for selecting items matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $query->addMetaDataFilter('type', $this->_type);
  }
}
