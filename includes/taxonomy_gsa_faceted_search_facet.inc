<?php

/**
 * @file
 * The taxonomy facet implementation for Google Search Appliance
 */

/**
 * A taxonomy-based facet.
 *
 * @see taxonomy_facet_category()
 */
class taxonomy_gsa_faceted_search_facet extends taxonomy_facet {

  /**
   * @param $search
   *  The faceted search object
   *
   * @return $category
   *  The category Array
   */
  function get_category($search) {
    $name = 'category-'. strtolower(str_replace(' ', '-', $this->_vocabulary->name));
    $terms = $this->_get_vocabulary_terms();
    return array(
      'category' => $name,
      'name' => $this->_vocabulary->name,
      'values' => $terms,
    );
  }

  /**
   * @return $users
   *  Array of terms
   */
  function _get_vocabulary_terms() {
    $terms = array();
    $tree = taxonomy_get_tree($this->_vocabulary->vid);
    foreach ($tree as $term) {
      $terms[$term->tid] = $term->name;
    }
    return $terms;
  }

 /**
  * Updates a query for retrieving the root categories of this facet and their
  * associated nodes within the current search results.
  *
  * @param $query
  *   The query object to update.
  *
  * @return
  *   FALSE if this facet can't have root categories.
  */
  function build_root_categories_query(&$query) {
    $name = 'category-'. strtolower(str_replace(' ', '-', $this->_vocabulary->name));
    $terms = $this->_get_vocabulary_terms();
    $query->addMetaDataFilter($name, $terms);
    return TRUE;
  }

 /**
  * This factory method creates categories given query results that include the
  * fields selected in get_root_categories_query() or get_subcategories_query().
  *
  * @param $results
  *   $results A database query result resource.
  *
  * @return
  *   Array of categories.
  */
  function build_categories($results) {
    $term = new stdClass();
    $categories = array();
    foreach ($results as $result) {
      foreach ($result as $key => $value) {
        $term->tid = $key;
        $term->vid = $this->_vocabulary->vid;
        $term->name = $value['value'];
        _taxonomy_facets_localize_term($term);
        $categories[] = new taxonomy_gsa_faceted_search_facet_category($term->tid, $term->name, $value['count']);
      }
    }
    return $categories;
  }
}

/**
 * A category for non-hierarchical taxonomy-based facets.
 *
 * @see taxonomy_facet()
 */
class taxonomy_gsa_faceted_search_facet_category extends taxonomy_facet_category {

 /**
  * Updates a query for selecting nodes matching this category.
  *
  * @param $query
  *   The query object to update.
  */
  function build_results_query(&$query) {
    $category = array();
    $term = taxonomy_get_term($this->_tid);
    $vocab = taxonomy_vocabulary_load($term->vid);
    $category['category'] = 'category-'. strtolower(str_replace(' ', '-', $vocab->name));
    $category['value'] = $term->name;
    $query->addMetaDataFilter($category['category'], $category['value']);
  }
}
