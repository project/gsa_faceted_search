<?php

/**
 * @file
 * The date authored facet implementation for Google Search Appliance
 */

/**
 * A facet for searching content by date of creation.
 */
class date_authored_gsa_faceted_search_facet extends date_authored_facet {

  /**
   * @param $search
   *  The faceted search object
   *
   * @return $category
   *  The category Array
   */
  function get_category($search) {
    $category = array();
    $category['category'] = 'created';
    $category['name'] = 'Date authored';
    $category['values'] = $this->_get_dates_created();
    return $category;
  }

  /**
   * @return $users
   *  Array of dates
   */
  function _get_dates_created() {
    $dates_created = array();
    $result = db_query('SELECT DISTINCT created FROM {node}');
    while ($date_created = db_fetch_object($result)) {
      $dates_created[] = date('Y-m-d h:i:s', $date_created->created);
    }
    return $dates_created;
  }

  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results.
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    $timezone = _date_authored_facet_get_timezone();
    
    // TODO: pass year into date_authored_facet_category
    $date_authored_facet_category = new date_authored_gsa_faceted_search_facet_category('2009');
    $year = sprintf("'%04d-01-01 00:00:00'", $date_authored_facet_category->get_year());
    $query->addMetaDataFilter('created', $year);
    return TRUE;
  }

 /**
  * This factory method creates categories given query results that include the
  * fields selected in get_root_categories_query() or get_subcategories_query().
  *
  * @param $results
  *   $results A database query result resource.
  *
  * @return
  *   Array of categories.
  */
  function build_categories($results) {
    $categories = array();
    foreach ($results as $result) {
      foreach ($result as $value) {
        $date = date_parse($value['value']);
        $categories[] = new date_authored_gsa_faceted_search_facet_category($date['year'], $date['month'], $date['day'], $value['count']);
      }
    }
    return $categories;
  }
}

/**
 * A category for node creation date.
 */
class date_authored_gsa_faceted_search_facet_category extends date_authored_facet_category {
  
  /**
   * Updates a query for retrieving the subcategories of this category and their
   * associated nodes within the current search results.
   *
   * This only needs to be overridden for hierarchical facets.
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have subcategories.
   */
  function build_subcategories_query(&$query) {
    if (isset($this->_day)) {
      return FALSE; // No subcategories.
    }
    if (isset($this->_month) || isset($this->_year)) {
      $this->build_results_query($query);
      return TRUE;
    }
    return FALSE; // Unreachable, unless something is wrong...
  }

  /**
   * Updates a query for selecting items matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $from = NULL;
    $to = NULL;
    $timezone = _date_authored_facet_get_timezone();

    if (isset($this->_day)) {
      $from = sprintf("%04d-%02d-%02d 00:00:00", $this->_year, $this->_month, $this->_day);
      $to = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($from)));
    }
    elseif (isset($this->_month)) {
      $from = sprintf("'%04d-%02d-01 00:00:00'", $this->_year, $this->_month);
      $to = sprintf("'%04d-%02d-01 00:00:00'", $this->_year, $this->_month + 1);
    }
    elseif (isset($this->_year)) {
      $from = sprintf("'%04d-01-01 00:00:00'", $this->_year);
      $to = sprintf("'%04d-01-01 00:00:00'", $this->_year + 1);
    }

    $query->addMetaDataFilter('created', 'daterange:'. $from .'...'. $to);
  }

  /**
   * $_year accessor
   */
  function get_year() {
    return $this->_year;
  }
}
