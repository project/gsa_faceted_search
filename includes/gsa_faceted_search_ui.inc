<?php

/**
 * @file
 * Search result styles implementation for Google Search Appliance
 */

 /**
  * Provides the 'extracts' display style for search results.
  */
class faceted_search_gsa_faceted_search_ui_extract_style extends faceted_search_ui_extract_style {

  /**
   * Format the search results.
   */
  function format_results($search) {
    return $search->load_results();
  }
}

/**
 * Provides the 'teasers' display style for search results.
 */
class faceted_search_gsa_faceted_search_ui_teaser_style extends faceted_search_ui_teaser_style {

  /**
   * Format the search results as extracts as teasers are not supported by GSA.
   * We need to implement this for faceted_search_ui compatibility reasons.
   */
  function format_results($search) {
    return $search->load_results();
  }
}
