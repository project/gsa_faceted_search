<?php

/**
 * @file
 * The author facet implementation for Google Search Appliance
 */

/**
 * A facet for node authors.
 */
class author_gsa_faceted_search_facet extends author_facet {

  /**
   * Constructor. Optionally assigns the active user of the facet.
   */
  function author_gsa_faceted_search_facet($excluded_roles = array(), $uid = 0, $name = '') {
    $active_path = array();
    if (is_numeric($uid) && $name) {
      $active_path[] = new author_google_appliance_facet_category($uid, $name);
    }
    parent::author_facet($excluded_roles, $uid, $name, $active_path);
  }

  /**
   * @param $search
   *  The faceted search object
   *
   * @return $category
   *  The category Array
   */
  function get_category($search) {
    $category = array();
    $category['category'] = 'author';
    $category['name'] = 'Author';
    $category['values'] = $this->_get_users();
    return $category;
  }

  /**
   * @return $users
   *  Array of users
   */
  function _get_users() {
    $users = array();
    $result = db_query('SELECT DISTINCT uid, name FROM {users}');
    while ($user = db_fetch_object($result)) {
      if ($user->name) {
        $users[$user->uid] = $user->name;
      }
      else {
        $users[] = t('anonymous');
      }
    }
    return $users;
  }

  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results.
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    $query->addMetaDataFilter('author', $this->_name);
    return TRUE;
  }

  /**
   *
   * @return
   *   Array of categories.
   */
  function build_categories($results) {
    $categories = array();
    foreach ($results as $result) {
      foreach ($result as $key => $value) {
        $categories[] = new author_gsa_faceted_search_facet_category($key, $value['value'], $value['count']);
      }
    }
    return $categories;
  }
}

/**
 * A node-type based facet category.
 */
class author_gsa_faceted_search_facet_category extends author_facet_category {
 
  /**
   * Updates a query for selecting items matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $query->addMetaDataFilter('author', $this->_name);
  }
}
